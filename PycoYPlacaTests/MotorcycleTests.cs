﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PicoYPLaca.VehicleClasses;
using System;

namespace PycoYPlacaTests
{
    [TestClass]
    public class MotorcycleTests
    {
        [TestMethod]
        public void EmptyPlateNumberInvalid()
        {
            Vehicle vehicle = new Motorcycle();
            Assert.AreEqual(vehicle.IsPlateNumberValid(), false);
        }

        [TestMethod]
        public void PlateNumberValidNoSeparator()
        {
            Vehicle vehicle = new Motorcycle("Monday", "PA1234", "07:30:00");
            Assert.AreEqual(vehicle.IsPlateNumberValid(), false);
        }

        [TestMethod]
        public void PlateNumberValidSeparatorIncomplete()
        {
            Vehicle vehicle = new Motorcycle("Monday", "P-1234", "07:30:00");
            Assert.AreEqual(vehicle.IsPlateNumberValid(), false);
        }

        [TestMethod]
        public void PlateNumberValid()
        {
            Vehicle vehicle = new Motorcycle("Monday", "PA-1234", "07:30:00");
            Assert.AreEqual(vehicle.IsPlateNumberValid(), true);
        }

         // Not Neccesary to add tests for checking if the motorcycle has restrictions or not
         // since that process is done in the base class "Vehicle" which is already tested as part
         // of the Car tests
    }
}
