﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PicoYPLaca.VehicleClasses;
using System;

namespace PicoYPlacaTests
{
    [TestClass]
    public class CarTests
    {
        [TestMethod]
        public void IsEmptyPLateNumberValid()
        {
            Vehicle vehicle = new Car();
            Assert.AreEqual(vehicle.IsPlateNumberValid(), false);
        }

        [TestMethod]
        public void IsPLateNumberValidNoSeparator()
        {
            Vehicle vehicle = new Car("Monday", "PAC1234", "07:30:00");
            Assert.AreEqual(vehicle.IsPlateNumberValid(), false);
        }

        [TestMethod]
        public void IsPLateNumberValidSeparatorIncomplete()
        {
            Vehicle vehicle = new Car("Monday", "P-1234", "07:30:00");
            Assert.AreEqual(vehicle.IsPlateNumberValid(), false);
        }

        [TestMethod]
        [DataRow("CX-1234", false)]
        [DataRow("CD-1234", true)]
        [DataRow("CC-1234", true)]
        [DataRow("AT-1234", true)]
        [DataRow("OI-1234", true)]
        public void IsPLateNumberValidDiplomatic(string plateNumber, bool valid)
        {
            Vehicle vehicle = new Car("Monday", plateNumber, "07:30:00");
            Assert.AreEqual(vehicle.IsVehicleDiplomatic(), valid);
        }

        [TestMethod]
        [DataRow("Monday", "07:00:00", "PXX-1231", false)]
        [DataRow("Monday", "09:30:00", "PXX-1231", false)]
        [DataRow("Monday", "16:00:00", "PXX-1231", false)]
        [DataRow("Monday", "19:30:00", "PXX-1231", false)]
        [DataRow("Monday", "06:30:00", "PXX-1231", true)]
        [DataRow("Monday", "09:31:00", "PXX-1231", true)]
        [DataRow("Monday", "15:30:00", "PXX-1231", true)]
        [DataRow("Monday", "19:31:00", "PXX-1231", true)]
        [DataRow("Monday", "07:00:00", "PXX-1232", false)]
        [DataRow("Tuesday", "07:00:00", "PXX-1233", false)]
        [DataRow("Tuesday", "07:00:00", "PXX-1234", false)]
        [DataRow("Wednesday", "07:00:00", "PXX-1235", false)]
        [DataRow("Wednesday", "07:00:00", "PXX-1236", false)]
        [DataRow("Thursday", "07:00:00", "PXX-1237", false)]
        [DataRow("Thursday", "07:00:00", "PXX-1238", false)]
        [DataRow("Friday", "07:00:00", "PXX-1239", false)]
        [DataRow("Friday", "07:00:00", "PXX-1230", false)]
        public void IsCarAllowedToDrive(string dayOfTheWeek, string time, string plateNumber, bool valid)
        {
            Vehicle vehicle = new Car(dayOfTheWeek, plateNumber, time);
            Assert.AreEqual(vehicle.IsVehicleAllowed(), valid);
        } 
    }
}
