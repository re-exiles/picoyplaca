﻿using PicoYPLaca.VehicleClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PicoYPLaca
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void OnClick(object sender, EventArgs e)
        {
            var dayOfTheWeek = dateTimePicker1.Value.DayOfWeek.ToString();
            var time = dateTimePicker2.Value.TimeOfDay.ToString().Split('.')[0];
            var plateNumber = textBox1.Text;

            ProcessInput(dayOfTheWeek, plateNumber, time);
        }

        private void ProcessInput(string dayOfTheWeek, string plateNumber, string time)
        {
            Vehicle vehicle;
            if (radioButtonCar.Checked)
            {
                vehicle = new Car(dayOfTheWeek, plateNumber, time);
            }
            else
            {
                vehicle = new Motorcycle(dayOfTheWeek, plateNumber, time);
            }


            if (!vehicle.IsPlateNumberValid())
            {
                var plateNumberMessage = "Plate number must be in the following format\n" +
                                         "Diplomatic: CD-1234\n" +
                                         "Particular: PXX-1234\n" +
                                         "Motorcycle: HC-123";
                
                var caption = "Wrong Plate Number format";

                MessageBox.Show(plateNumberMessage, caption);
                return;
            }

            if (vehicle.IsVehicleDiplomatic() || vehicle.IsVehicleAllowed())
            {
                var plateNumberMessage = $"Your Vehicle with plate number ({vehicle.PlateNumber}) is allowed to go out";

                var caption = "Valid Plate Number - NO RESTRICTION";

                MessageBox.Show(plateNumberMessage, caption);
                return;
            }
            else
            {
                var plateNumberMessage = $"Your Vehicle with plate number ({vehicle.PlateNumber}) is not allowed to go out";

                var caption = "Valid Plate Number - RESTRICTION";

                MessageBox.Show(plateNumberMessage, caption);
                return;
            }
        }
    }
}
