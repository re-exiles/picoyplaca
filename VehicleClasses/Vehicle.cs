﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoYPLaca.VehicleClasses
{
    public abstract class Vehicle
    {
        public string DayOfTheWeek { get; private set; }
        public string PlateNumber { get; private set; }
        public string Time { get; private set; }
        
        // Will be used to check the correct format for each plate number
        protected char plateNumberSeparator = '-';

        // Pico Y Placa rules
        private List<(string, int[])> rules = new List<(string, int[])>
        {
            ("Monday",    new int[] {1, 2}),
            ("Tuesday",   new int[] {3, 4}),
            ("Wednesday", new int[] {5, 6}),
            ("Thursday",  new int[] {7, 8}),
            ("Friday",    new int[] {9, 0}),
        };

        // Pico Y Placa time restrictions
        private List<(string, string)> timeRestrictions = new List<(string, string)>
        {
            ("07:00:00", "09:30:00"),
            ("16:00:00", "19:30:00"),
        };

        public Vehicle()
        {
            DayOfTheWeek = "";
            PlateNumber = "";
            Time = "";
        }

        public Vehicle(string dayOfTheWeek, string plateNumber, string time)
        {
            this.DayOfTheWeek = dayOfTheWeek;
            this.PlateNumber = plateNumber;
            this.Time = time;
        }

        // Not all vehicles have diplomatic plate numbers, so leaving false as default
        public virtual bool IsVehicleDiplomatic()
        {
            return false;
        }

        // Must implement a valid plate number check for a vehicle in the Derived Classes
        public abstract bool IsPlateNumberValid();

        // All derived vehicles (Cars, Motorcicles, etc) will have the same check for their plate numbers
        public bool IsVehicleAllowed()
        {
            if (!IsPlateNumberValid())
            {
                return false;
            }

            string[] plateNumberSections = PlateNumber.Split(plateNumberSeparator);
            var lastDigitPlateNumber = Int32.Parse(plateNumberSections[1].Substring(plateNumberSections[1].Length - 1));
            var platenumberRestriction = rules.Where(dayRule => dayRule.Item1.Equals(DayOfTheWeek)).
                                               ToList()[0].
                                               Item2.Any(digit => digit == lastDigitPlateNumber);

            var dateTime = DateTime.Parse(Time);
            var timeRestriction = timeRestrictions.Any(timeGap => DateTime.Parse(timeGap.Item1) <= dateTime && DateTime.Parse(timeGap.Item2) >= dateTime);
            
            if (platenumberRestriction && timeRestriction)
            {
                return false;
            }

            return true;
        }
    }
}
