﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoYPLaca.VehicleClasses
{
    public class Motorcycle : Vehicle
    {
        public Motorcycle() : base()
        {

        }

        public Motorcycle(string dayOfTheWeek, string plateNumber, string time) : base(dayOfTheWeek, plateNumber, time)
        {

        }

        public override bool IsPlateNumberValid()
        {
            // No Plate number specified
            if (String.IsNullOrEmpty(PlateNumber))
            {
                return false;
            }

            string[] plateNumberSections = PlateNumber.Split(plateNumberSeparator);

            if (!(plateNumberSections[0].Length == 2))
            {
                return false;
            }

            // Check if first section of the plate number has only letters
            if (!(plateNumberSections[0].All(Char.IsLetter)))
            {
                return false;
            }

            // Check if second section of the plate number has only numbers
            if (!(plateNumberSections[1].All(Char.IsNumber)))
            {
                return false;
            }

            return true;
        }
    }
}
