﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoYPLaca.VehicleClasses
{
    public class Car : Vehicle
    {
        List<string> diplomaticPlates = new List<string>()
        {
            "CD",
            "CC",
            "AT",
            "OI"
        };

        public Car() : base()
        {

        }

        public Car(string dayOfTheWeek, string plateNumber, string time) : base(dayOfTheWeek, plateNumber, time)
        {

        }

        public override bool IsPlateNumberValid()
        {
            // No Plate number specified
            if (String.IsNullOrEmpty(PlateNumber))
            {
                return false;
            }

            string[] plateNumberSections = PlateNumber.Split(plateNumberSeparator);

            // Check if first section of the plate number has a lenght of 2 or 3 characters
            // 2 characters: Diplomatic vehicles
            // 3 characters: Normal Vehicles
            if (!(plateNumberSections[0].Length == 2 || plateNumberSections[0].Length == 3))
            {
                return false;
            }

            // Check if first section of the plate number has only letters
            if (!(plateNumberSections[0].All(Char.IsLetter)))
            {
                return false;
            }

            // Check if second section of the plate number has only numbers
            if (!(plateNumberSections[1].All(Char.IsNumber)))
            {
                return false;
            }

            return true;
        }

        public override bool IsVehicleDiplomatic()
        {
            string[] plateNumberSections = PlateNumber.Split(plateNumberSeparator);

            // Diplomatic vehicles:
            // * Have only 2 letters in the first section of the plate number
            // * Have any letter combination from the list diplomaticPlates
            if (plateNumberSections[0].Length == 2 && 
                diplomaticPlates.Any(a => a == plateNumberSections[0].ToUpper()) &&
                plateNumberSections[0].All(Char.IsLetter))
            {
                return true;
            }

            return false;
        }
    }
}
