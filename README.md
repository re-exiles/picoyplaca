# Pico y Placa DEMO

### **Done by:** Erick Almeida

### **Working Environment**
* .NET Framework 4.7.2
* VS2019 - Version 16.8.4
* C# - version depends on framework version

### **How to run the program**
* This program can only run in WINDOWS.
* Clone this repo
* Open "PicoYPLaca.sln" in VS2019 (Haven't tested on previos versions of VS).
* The solution contains the program project and the unittest project.
* Run the program. A GUI will appear (I made the program using Winforms)
* To run the unittests, right click on the unittest project, and select "Run Tests".

### **NOTE**
* If the plate number has the incorrect format, a pop up window will appear with some information on how to properly write the plate number for a car and motorcycle.
* Plate number must be in the following format
  - Diplomatic: CD-1234
  - Particular: PXX-1234
  - Motorcycle: HC-123
* A pop up window will also appear to inform if the vehicle entered in the main window has some restrictions of PICO Y PLACA or not.
* Give it a try, didn't want to spoil much of what the program is checking.
* For any feedback, please write to my email almeida.ericks29@gmail.com